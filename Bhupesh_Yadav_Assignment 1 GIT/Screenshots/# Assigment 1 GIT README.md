# Assigment 1 (Git)
 
 
 
# Initialize a local git repository
 
git init Node_project
# Run git status command
git status
On branch master
No commits yet 
 
 
# Create a file README.md
touch README.md
 
# Run git status command
git status
On branch master
 
No commits yet
 
Untracked files:
  (use "git add <file>..." to include in what will be committed)
 
	README.md
 ![image](Screenshots/a.png)
 
# Add file README.md in your local repository
git add README.md 
# Add My first interaction with Git content in README.md
echo  My first interaction with Git > README.md
 
# Run git status command
git status
On branch master
 
No commits yet
 
Changes to be committed:
  (use "git rm --cached <file>..." to unstage)
 
	new file:   README.md
 
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)
 
	modified:   README.md
 
 
# Update file README.md in your local repository
machine1@ubuntu:~/Desktop/14-10-2019/Node_testing$ git commit -m " Added to Local repository "
[master (root-commit) 006753f]  Added to Local repository
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 README.md
 
 
# Run git status command
git status
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)
 
	modified:   README.md
 
no changes added to commit (use "git add" and/or "git commit -a")
 
 
# Create files README1.md README2.md README3.md README4.md
machine1@ubuntu:~/Desktop/14-10-2019/Node_testing$ touch README1.md README2.md README3.md README4.md
 
 
# Run git status command
git status
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)
 
	modified:   README.md
 
Untracked files:
  (use "git add <file>..." to include in what will be committed)
 
	README1.md
	README2.md
	README3.md
	README4.md
 
no changes added to commit (use "git add" and/or "git commit -a")
 
 
# Add all the newly added files in your local repository without giving their name.
machine1@ubuntu:~/Desktop/14-10-2019/Node_testing$ git add .
 
 
# Run git status command
machine1@ubuntu:~/Desktop/14-10-2019/Node_testing$ git status
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)
 
	modified:   README.md
	new file:   README1.md
	new file:   README2.md
	new file:   README3.md
	new file:   README4.md
 
 
# Remove files README1.md README2.md from local repository
machine1@ubuntu:~/Desktop/14-10-2019/Node_testing$ git status
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)
 
	deleted:    README1.md
	deleted:    README2.md
 
 
# Add content in Temporary content in README3.md
machine1@ubuntu:~/Desktop/14-10-2019/Node_testing$ echo temp content > README3.md 
# Run git status command
machine1@ubuntu:~/Desktop/14-10-2019/Node_testing$ git status
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)
 
	deleted:    README1.md
	deleted:    README2.md
 
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)
 
	modified:   README3.md
 
 
# Find out the content that is added in README3.md
machine1@ubuntu:~/Desktop/14-10-2019/Node_testing$ git diff README3.md
diff --git a/README3.md b/README3.md
index e69de29..de14eb1 100644
--- a/README3.md
+++ b/README3.md
@@ -0,0 +1 @@
+temp content
 
 
 
# Undo the changes in README3.md file
machine1@ubuntu:~/Desktop/14-10-2019/Node_testing$ git reset README3.md
Unstaged changes after reset:
M	README3.md
 
 
# Run git status command
machine1@ubuntu:~/Desktop/14-10-2019/Node_testing$ git status
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)
 
	deleted:    README1.md
	deleted:    README2.md
 
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)
 
	modified:   README3.md
 
 
 
# Create a branch ninja from current branch
machine1@ubuntu:~/Desktop/14-10-2019/Node_testing$ git branch ninja
 
 
# Create sensei branch from ninja branch and switch to sensei branch
machine1@ubuntu:~/Desktop/14-10-2019/Node_testing$ git checkout ninja
M	README3.md
Switched to branch 'ninja'
machine1@ubuntu:~/Desktop/14-10-2019/Node_testing$ git branch sensei
 
 
# List out all the local branches
machine1@ubuntu:~/Desktop/14-10-2019/Node_testing$ git branch
  master
* ninja
  sensei
 
# Delete sensei branch
machine1@ubuntu:~/Desktop/14-10-2019/Node_testing$ git branch -d sensei
Deleted branch sensei (was d83283e).
 
 
# Find out the current branch
machine1@ubuntu:~/Desktop/14-10-2019/Node_testing$ git branch
  master
* ninja  <--- This * denote your current branch
 
 
# List out all the commits that have been done
machine1@ubuntu:~/Desktop/14-10-2019/Node_testing$ git log
commit d83283e939f87ec7daf385a5f5040a9eb2c3a7f8 (HEAD -> ninja, master)
Author: Bhupesh Yadav <bhupesh.yadav@mygurukulam.org>
Date:   Sun Oct 13 23:28:28 2019 -0700
 
     Stagged all the data
 
commit b2e0b5da83b5863c9584e425a493824cd1ea6b64
Author: Bhupesh Yadav <bhupesh.yadav@mygurukulam.org>
Date:   Sun Oct 13 23:22:27 2019 -0700
 
     4 files are added to local repository
 
commit 006753f87686e56441153d6cf34f30c06d5e4636
Author: Bhupesh Yadav <bhupesh.yadav@mygurukulam.org>
Date:   Sun Oct 13 23:16:31 2019 -0700
 
     Added to Local repository
machine1@ubuntu:~/Desktop/14-1
 
 
 
Good To Do
 
Create a shell script that will take a Git URL Number of days as input and it will list out all the commits that have been done in last n number of days. The script should display information like given below. A commit will be treated as valid one if it starts with a JIRA id [JIRA-xxxx]:
 
 
Date/Time, Author Name, Author Email, Commit Message, Valid Commit Message
 
Enable a functionality in your local repository to not allow a commit if it follows above condition of a valid commit message
 
 
Learning
In this section you learned all the basic commands of git to manage local repository
 
init → It will initialize a bare repository in current directory.
add  → use to add files
status It will show all tracked and untracked file on your repository
commit :- Send your files to your staging area
rm it will delete files
diff show difference of selected files
checkout :- used to change branches
branch :- to perform brancg operation Add, Delete ,
 
9:38 PM
Assignment 2 (Git) 30K Feet View
Introduction
In this section we will build understanding around git remote repo management
References
 
https://git-scm.com/docs
https://github.com/joshnh/Git-Commands
 
 
Assignments
 
Must Do
 
Create a GitLab account if it doesn't exists already.
Done
Create a repo GitCommand in your GitLab account .
Delete repo *GitCommand"
Fork https://github.com/joshnh/Git-Commands in your GitLab account with the name GitCommand
 
Clone GitCommand repo using http protocol
Delete the cloned code
Clone GitCommand repo using git protocol at ~/ninja
Create a folder ninja
 
Create a file README.md in ninja folder
Make your changes available to GitCommand remote repo
 
 
Good To Do
 
Get the latest code from remote repo without using git pull command
Simulate above problem where instead of GitLab you would be using Git file server
Simulate above problem where instead of GitLab you would be using custom Git SSH server
 
 
Learning
In this section you learned all the basic commands of git to manage local repository
 
init
add
status
commit
rm
diff
checkout
branch

