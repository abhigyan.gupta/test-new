# Assignments


# Suppose you have two branches say branch A and branch B. Currently you are working in Branch B.

```
machine1@ubuntu:~/Desktop/15-10-2019/assignment2_staging$ git branch Branch_A
machine1@ubuntu:~/Desktop/15-10-2019/assignment2_staging$ git branch Branch_B
machine1@ubuntu:~/Desktop/15-10-2019/assignment2_staging$ git branch
  Branch_A
  Branch_B
  master *  
machine1@ubuntu:~/Desktop/15-10-2019/assignment2_staging$ git checkout Branch_B
Switched to branch 'Branch_B'
machine1@ubuntu:~/Desktop/15-10-2019/assignment2_staging$ git branch
  Branch_A
  Branch_B *   <-----Working Branch
  master
	
```

![image](Screenshots/a.png)

# Create any file with any content in branch B and add it in staging area but dont commit it.

```
machine1@ubuntu:~/Desktop/15-10-2019/assignment2_staging$ git branch
  Branch_A
* Branch_B
  master
machine1@ubuntu:~/Desktop/15-10-2019/assignment2_staging$ touch New.txt
machine1@ubuntu:~/Desktop/15-10-2019/assignment2_staging$ git add New.txt 
machine1@ubuntu:~/Desktop/15-10-2019/assignment2_staging$ git status
On branch Branch_B
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

	new file:   New.txt

```
![image](Screenshots/b.png)



# Try to switch to Branch A. If its failing, then find a way out to switch to Branch A without commiting file in branch B.
```
Switched Normally without any error

```
![image](Screenshots/c.png)

# Read about git reset (Soft, Mixed and Hard) command and implement it in your local git repository. And write your observations in each method (Soft, Mixed and Hard) Note: Dont copy paste from the internet. Write it in very simple language.

```
Git-Reset
Hard reset
Soft reset
Mixed reset


```

# Create a shell script which will take a number "n" as a input from user and script will revert the changes upto "n" no of commits. And if "n" is greater than no of commits in the branch in your git repository, then it should print "Invalid input (Input provided is greater than no of commits in the branch)"

```
Simple Script is created 

```
![image](Screenshots/revert.png)

# Create a shell script which will take below inputs from user
#  Remove untracked file (yes/No)

```
read -p " You want clean the Untracked Directories YES..NO " INPUT

if
        [ $INPUT = "YES" ]; then

        git clean -f

else
        echo " Bad!!!#Input  We are not cleaning anything "

fi

```

![image](Screenshots/cleanfilee.png)

# Remove untracked directories (yes/No)
# and script will remove files or directories from git repo acc to the inputs given by the user.

```
read -p " You want clean the Untracked Directories YES..NO " INPUT

if
        [ $INPUT = "YES" ]; then

        git clean -f -d

else
        echo " Bad!!!#Input  We are not cleaning anything "

fi




```
![image](Screenshots/directo.png)


# Good To Do



# Change git log format to below format:
# <commit hash(red colour)> - <commit message(blue color)> - <commiter_date'''


# Read about Git Hooks.
Using Git hooks vaildate the commit message. Eg: If any developer commits anything , then commit message should start from "Opstree--" string, otherwise it should print "Invalid commit message. Please start you ur commit msg with Opstree--"
'''

'''

