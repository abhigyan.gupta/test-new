# We will continue with our GitCommand remote repository

# Must Do

# Create a folder ninja at the root level of your cloned code

```

machine1@ubuntu:~/Desktop/15-10-2019/Git-Commands$ mkdir ninja
machine1@ubuntu:~/Desktop/15-10-2019/Git-Commands$ ls
ninja  README.md  READMEpt.md

```
# Add a file README.md with content "Trying fast forward merge"

```
machine1@ubuntu:~/Desktop/15-10-2019/Git-Commands$ ls
ninja  README.md

```
![image](Screenshots/1echo.png)

# Create a branch ninja and move to it
```

```
![image](Screenshots/1gitcheckout.png)

#Run git status command

![image](Screenshots/1status.png)



#Commit your changes to ninja branch

![image](Screenshots/1status2.png)

![image](Screenshots/1commit.png)

#Merge ninja branch to master branch make sure that a new commit get's created


![image](Screenshots/1mercommitmaster)

#Assuming you are in master branch, modify README.md with content Changes in master branch, commit the changes in master branc

![image](Screenshots/1commitmaster.png)





#Switch to ninja branch, modify README.md with content Changes in ninja branch, commit the changes in ninja branch.

![image](Screenshots/1gitcommitninja.png)



#Merge master branch to ninja branch in such a fashion that changes of master branch overrides changes in ninja branch

![image](Screenshots/1mergingnew.png)

#Revert the above merge commit

![image](Screenshots/1revert1.png)	
